# 2022 Stem Cell Network RNASeq Workshop

### R Language Introduction: 
* May 2, 11-2 PM (EDT)

## Workshop Dates (June)
* Session 1: May 5, 6 Tutorial Day May 9, 11-5PM (EDT)
* Session 2: May 26, 27 Tutorial Day May 30, 11-5PM (EDT)
* Session 3: June 16, 17 Tutorial Day June 20, 11-5PM (EDT)

## Workshop Venue

A Zoom meeting link for each session will be sent to participants separately.

## Organizers		
Dr. Bill Stanford (uOttawa/OHRI)  
Dr. Ted Perkins (uOttawa/OHRI)  

## Workshop Files

* [Workshop Agenda](https://gitlab.com/ohri/2022-scn-rnaseq-workshop/-/raw/main/Files/2022_SCN_RNA-seq_workshop_agenda.pdf?inline=false)


### R Language Introduction

* [R walkthrough commands](Files/Rwalkthrough.Rmd) 
    * [video](http://dropbox.ogic.ca/workshop_2022/Rintro2022.mp4)
* Introduction to R Packages ([PowerPoint](Files/Intro_to_R_Packages.pptx?inline=false))
* Library installation required for Day 1 & 2 exercises [Document](Files/R_package_installation.txt).

### RNASeq Workshop

### Day 1:

Workshop and Goals (Dr. Bill Stanford) [PowerPoint](https://gitlab.com/ohri/2021-scn-rnaseq-workshop/-/blob/main/Files/RNA-seq_workshop_Stanford_2021.pptx), [Video](http://dropbox.ogic.ca/workshop_2022/SCN_2022_Day1_Session1_Lecture1_Stanford.mp4) 
* _presentation video from Oct 2021 used as Dr. Stanford was unavailable to present_

RNA-seq Basics (Gareth Palidwor) [PowerPoint](Files/2022_Workshop_Lecture_1_RNASeq_Basics.pptx?inline=false), _Video unavailable_
* Example multiqc file
    * [MultiQC report](http://www.ogic.ca/projects/workshop_2019/multiqc/multiqc_report.html?infile=false)
* Example BAM files for [IGV](Files/IGV.md) visualization
    * [in_day1_rep1_chr12.bam](https://gitlab.com/ohri/2019-SCN-rnaseq-workshop/-/blob/master/Files/in_day1_rep1_chr12.bam?inline=false)
    * [in_day1_rep1_chr12.bam.bai](https://gitlab.com/ohri/2019-SCN-rnaseq-workshop/-/blob/master/Files/in_day1_rep1_chr12.bam.bai?inline=false)

Differential Gene Expression with DESeq2 (Chris Porter) [PowerPoint](Files/2022_Workshop_Lecture_2_DESeq2.pptx?inline=false), [Lecture Video](http://dropbox.ogic.ca/workshop_2022/deseq2_2022.mp4),  [Exercises Video](http://dropbox.ogic.ca/workshop_2022/DESeq2_exercise.mp4)
* [Salmon gene counts (MOV10) for DE analysis](Files/salmon_MOV10.zip?inline=false)
* [R Script for Differential Gene Expression Analysis](Files/DESeq2_analysis_script.Rmd?inline=false)
* [Transcript to gene mapping](Files/tx2gene_grch38_ens94.txt?inline=false)

Gene Set Enrichment Analysis (Gareth Palidwor) [PowerPoint](Files/2022_Workshop_Lecture_3.ppt?inline=false),[Video](http://dropbox.ogic.ca/workshop_2022/enrichment_2022.mp4) 
* [g:Profiler exercise instructions](Files/gprofiler.md) 


RNA-seq Experimental Design  (Dr. Ted Perkins) 
* May 5: [Presentation](Files/Perkins_RNASeqWorkshop_2022.pdf), [Video](http://dropbox.ogic.ca/workshop_2022/Perkins_lecture_2022.mp4) 
* May 26: [Presentation](Files/Perkins_RNASeqWorkshop_2022_v2b.pdf)
* June 16: [Presentation](Files/Perkins_RNASeqWorkshop_2022_v3.pdf)

### Day 2:

Introduction to the single-cell RNA sequencing workflow (Dr. David Cook) [Presentation](https://gitlab.com/ohri/2021-scn-rnaseq-workshop/-/blob/main/Files/scRNAseq_workshop_ppt.pptx), [Video](http://dropbox.ogic.ca/workshop_2022/SingleCellLecture.mp4)


[Single-cell data and notebooks](https://github.com/dpcook/scrna_seq_workshop_2021), [Video of script walkthrough](http://dropbox.ogic.ca/workshop_2022/David_tutorial.mp4)

